import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
ImageArray=[];

  slides1 = {
    slidesPerView: 1.6,
    spaceBetween: 10
  };

  slides = {
    autoplay: true,
    loop: true
  };

  constructor(public navCtrl: NavController) {
  this.ImageArray =[
  {'image' : 'assets/gambar/hm1.jpg'},
  {'image' : 'assets/gambar/cfc.jpg'},
  {'image' : 'assets/gambar/ricis.jpg'}];
  }

}
