import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children:[
    	{
    		path:'home',
    		children:[
    			{
    				path:'',
    				loadChildren: ()=>
    					import('../home/home.module').then (m => m.HomePageModule)
    			}
    		]
    	},
    	{
    		path:'menumakan',
    		children:[
    			{
    				path:'',
    				loadChildren: ()=>
    					import('../menumakan/menumakan.module').then (m => m.MenumakanPageModule)
    			}
    		]
    	},
        {
            path:'permintaan',
            children:[
                {
                    path:'',
                    loadChildren: ()=>
                        import('../permintaan/permintaan.module').then (m => m.PermintaanPageModule)
                }
            ]
        },
        {
            path:'akun',
            children:[
                {
                    path:'',
                    loadChildren: ()=>
                        import('../akun/akun.module').then (m => m.AkunPageModule)
                }
            ]
        },
        {
            path:'pembayaran',
            children:[
                {
                    path:'',
                    loadChildren: ()=>
                        import('../pembayaran/pembayaran.module').then (m => m.PembayaranPageModule)
                }
            ]
        },
        {


            path:'pemesanan',
            children:[
                {
                    path:'',
                    loadChildren: ()=>
                        import('../pemesanan/pemesanan.module').then (m => m.PemesananPageModule)
                }
            ]
        },
        {
            path:'profil',

            children:[
                {
                    path:'',
                    loadChildren: ()=>
                        import('../profil/profil.module').then (m => m.ProfilPageModule)
                }
            ]

        },
        {
            path:'listresto',
            children:[
                {
                    path:'',
                    loadChildren: ()=>
                        import('../listresto/listresto.module').then (m => m.ListrestoPageModule)
                }
            ]

        },
        {
            path:'aktivitas',
            children:[
                {
                    path:'',
                    loadChildren: ()=>
                        import('../aktivitas/aktivitas.module').then (m => m.AktivitasPageModule)
                }
            ]
        }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
