import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenumakanPageRoutingModule } from './menumakan-routing.module';

import { MenumakanPage } from './menumakan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenumakanPageRoutingModule
  ],
  declarations: [MenumakanPage]
})
export class MenumakanPageModule {}
