import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenumakanPage } from './menumakan.page';

const routes: Routes = [
  {
    path: '',
    component: MenumakanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenumakanPageRoutingModule {}
