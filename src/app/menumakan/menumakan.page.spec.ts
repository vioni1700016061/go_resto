import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenumakanPage } from './menumakan.page';

describe('MenumakanPage', () => {
  let component: MenumakanPage;
  let fixture: ComponentFixture<MenumakanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenumakanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenumakanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
