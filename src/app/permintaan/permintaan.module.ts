import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PermintaanPageRoutingModule } from './permintaan-routing.module';

import { PermintaanPage } from './permintaan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PermintaanPageRoutingModule
  ],
  declarations: [PermintaanPage]
})
export class PermintaanPageModule {}
