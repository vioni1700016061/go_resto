import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PermintaanPage } from './permintaan.page';

describe('PermintaanPage', () => {
  let component: PermintaanPage;
  let fixture: ComponentFixture<PermintaanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermintaanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PermintaanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
