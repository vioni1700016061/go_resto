import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PermintaanPage } from './permintaan.page';

const routes: Routes = [
  {
    path: '',
    component: PermintaanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PermintaanPageRoutingModule {}
