import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-permintaan',
  templateUrl: './permintaan.page.html',
  styleUrls: ['./permintaan.page.scss'],
})
export class PermintaanPage implements OnInit {

  slides = {
	  slidesPerView: 2.6,
	  spaceBetween: 5
  };

  constructor() { }

  ngOnInit() {
  }

}
