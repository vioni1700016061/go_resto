import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then( m => m.TabsPageModule)
  },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'menumakan',
    loadChildren: () => import('./menumakan/menumakan.module').then( m => m.MenumakanPageModule)
  },
  {
    path: 'permintaan',
    loadChildren: () => import('./permintaan/permintaan.module').then( m => m.PermintaanPageModule)
  },
  {
    path: 'tabs1',
    loadChildren: () => import('./tabs1/tabs1.module').then( m => m.Tabs1PageModule)
  },
  {
    path: 'akun',
    loadChildren: () => import('./akun/akun.module').then( m => m.AkunPageModule)
  },
  {
    path: 'profil',
    loadChildren: () => import('./profil/profil.module').then( m => m.ProfilPageModule)
  },
  {
    path: 'pembayaran',
    loadChildren: () => import('./pembayaran/pembayaran.module').then( m => m.PembayaranPageModule)
  },
  {
    path: 'pemesanan',
    loadChildren: () => import('./pemesanan/pemesanan.module').then( m => m.PemesananPageModule)
  },
  {
    path: 'listresto',
    loadChildren: () => import('./listresto/listresto.module').then( m => m.ListrestoPageModule)
  },
  {
    path: 'aktivitas',
    loadChildren: () => import('./aktivitas/aktivitas.module').then( m => m.AktivitasPageModule)
  }




  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
