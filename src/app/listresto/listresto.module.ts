import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListrestoPageRoutingModule } from './listresto-routing.module';

import { ListrestoPage } from './listresto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListrestoPageRoutingModule
  ],
  declarations: [ListrestoPage]
})
export class ListrestoPageModule {}
