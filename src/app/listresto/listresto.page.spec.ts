import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListrestoPage } from './listresto.page';

describe('ListrestoPage', () => {
  let component: ListrestoPage;
  let fixture: ComponentFixture<ListrestoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListrestoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListrestoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
