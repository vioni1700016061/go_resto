import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-listresto',
  templateUrl: './listresto.page.html',
  styleUrls: ['./listresto.page.scss'],
})
export class ListrestoPage {

jsondata:any;

  constructor(public navCtrl: NavController) {
  this.initialjsondata();
  }

  filterjson(ev:any){
    this.initialjsondata();
    const val=ev.target.value;
    if(val && val.trim() != '')
    {
      this.jsondata = this.jsondata.filter((item)=>{
        return (item.title.toLowerCase().indexOf(val.toLowerCase())>-1);
      })
    }
  }

  selectVal(val){
    this.navCtrl.navigateForward('/profil');
  }

  initialjsondata()
  {
    this.jsondata = [
      {
        "title" : "Spesial Sambal",
        "lokasi" : "Jl veteran",
        "img" : "assets/ss.jpg"
      },
      {
        "title" : "Solaria",
        "lokasi" : "Plaza Ambarukmo",
        "img" : "assets/solaria.jpg"
      },
      {
        "title" : "KFC",
        "lokasi" : "KFC Mall Malioboro",
        "img" : "assets/kfc.jpg"
      },
      {
        "title" : "Nasi Padang Jaya Basamo",
        "lokasi" : "Jl Sorogenen",
        "img" : "assets/padang.jpg"
      }
    ];
  }

  profil(){
    this.navCtrl.navigateForward('/profil');
  }
}
