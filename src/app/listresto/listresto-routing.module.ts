import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListrestoPage } from './listresto.page';

const routes: Routes = [
  {
    path: '',
    component: ListrestoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListrestoPageRoutingModule {}
